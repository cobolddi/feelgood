<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package feelgoodCobold
 */

?>

	</main>

	<footer>
		<div class="container">
			<div class="TopFooterMenu">
				<div class="row">
					<div class="col-12 col-md-3">
						<a href=""><img src="<?php $logo = get_field('footer_logo', 'option'); echo $logo['url'] ?>"></a>
						<?php the_field('footer_top1_content', 'option') ?>
					</div>
					<div class="col-12 col-md-2">
						<?php the_field('footer_top2_content', 'option') ?>
						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-2',
								'menu_id'        => 'footer-1-menu',
							) );
						?>
					</div>
					<div class="col-12 col-md-3">
						<?php the_field('footer_top3_content', 'option') ?>
						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-3',
								'menu_id'        => 'footer-2-menu',
							) );
						?>
					</div>
					<div class="col-12 col-md-4">
						<?php the_field('footer_top4_content', 'option') ?>
						 <?php echo do_shortcode('[email-subscribers-form id="2"]'); ?>
						<?php 
							// echo do_shortcode('[contact-form-7 id="137" title="Email Subscriber"]'); 
						?>
						<!-- <form>
							<input type="email" name="" placeholder="Your email address">
							<input type="submit" name="" value="Subscribe">
						</form> -->
					</div>
				</div>
			</div>
			<div class="BottomFooterMenu">
				<div class="row">
					<div class="col-12 col-md-3">
						<?php the_field('footer_bottom1_content', 'option') ?>
						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-4',
								'menu_id'        => 'footer-3-menu',
							) );
						?>
						
						<ul class="SocialIcons">
							<li>
								Follow Us :
							</li>
							<?php
								if( have_rows('social_menu', 'option') ):
									while( have_rows('social_menu', 'option') ) : the_row();
								?>
							<li>
								<a href="<?php $link = get_sub_field('menu_item_link'); echo $link['url']; ?>" target="_blank">
									<svg>
										<use xlink:href="<?php echo get_template_directory_uri() ?>/assets/img/feelgood.svg#<?php the_sub_field('menu_item_id') ?>"></use>
									</svg>
								</a>
							</li>
							<?php
									endwhile;
								endif;
							?>					

						</ul>
					</div>
					<div class="col-12 col-md-2">
						
						<?php the_field('footer_bottom2_content', 'option') ?>
						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-5',
								'menu_id'        => 'footer-4-menu',
							) );
						?>
					</div>
					<div class="col-12 col-md-3">
						<div class="row">
							<div class="col-12 col-md-6">
								<?php the_field('footer_bottom3_content', 'option') ?>
								<?php
									wp_nav_menu( array(
										'theme_location' => 'menu-6',
										'menu_id'        => 'footer-5-menu',
									) );
								?>
							</div>
							<div class="col-12 col-md-6">
								<?php the_field('footer_bottom4_content', 'option') ?>
								<?php
									wp_nav_menu( array(
										'theme_location' => 'menu-7',
										'menu_id'        => 'footer-6-menu',
									) );
								?>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-4">
						<div class="row">
							<div class="col-12 col-md-4">
								<?php the_field('footer_bottom5_content', 'option') ?>
								<?php
									wp_nav_menu( array(
										'theme_location' => 'menu-8',
										'menu_id'        => 'footer-7-menu',
									) );
								?>
							</div>
							<div class="col-12 col-md-8">
								<h4>Get In Touch</h4>
								<ul class="ContactDetails">
									<li><img src="<?php echo get_template_directory_uri(); ?>/assets/img/map.png"> <?php the_field('address', 'option') ?></li>
									<li>
										<a href="tel:+91 11 4659-4898">
											<img src="<?php echo get_template_directory_uri(); ?>/assets/img/phone.png"><?php the_field('phone_number', 'option') ?>
										</a>
									</li>
									<li>
										<a href="mailto:<?php the_field('email', 'option') ?>">
											<img src="<?php echo get_template_directory_uri(); ?>/assets/img/email.png"><?php the_field('email', 'option') ?>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="CopyrightBlock">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-10">
						<p>Copyright <i class="fa fa-copyright" aria-hidden="true"></i> 2019 Feel Good. All rights reserved.</p>
					</div>
					<div class="col-12 col-md-2">
						<p>Credit: <a href="cobold.co" target="_blank">Cobold</a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
