<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package feelgoodCobold
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header id="navbar">
		<div class="container">
			<a href=""><img class="Logo" src="<?php $logo = get_field('header_logo', 'option'); echo $logo['url']; ?>"></a>
		</div>
		<div class="HeaderMenuBox">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-12">
						<div class="MenuHeader">
							<nav>
								<div class="DesktopMenu">
									<?php
										wp_nav_menu( array(
											'theme_location' => 'menu-1',
											'menu_id'        => 'primary-menu',
										) );
									?>
								</div>
								
								<div class="MobileMenu">
								<!-- Mobile Menu -->
									<button class="grt-mobile-button">
								        <div class="line1"></div>
								        <div class="line2"></div>
								        <div class="line3"></div>
								    </button>
								    <ul>
								        <li class="active"><a href="">Home</a></li>
								        <li>
											<a href="">Sleep Systems</a>
											<ul>
												<li><a href="">Adjustable Beds</a></li>
												<li><a href="">Mattress</a></li>
												<li><a href="">Pillows</a></li>
											</ul>
										</li>
								        <li><a href="">Quality of Sleep</a></li>
								        <li>
											<a href="">Why Choose Feel Good</a>
											<ul>
												<li><a href="">USP</a></li>
												<li><a href="">Testimonials</a></li>
												<li><a href="">Sitemap</a></li>
											</ul>
										</li>
								        <li>
											<a href="">About Us</a>
											<ul>
												<li><a href="">Profile</a></li>
												<li><a href="">Franchise & Partnership</a></li>
												<li><a href="">Dealex</a></li>
											</ul>
										</li>
								        <li><a href="">Contact Us</a></li>
								    </ul>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<?php
		
			$headerType = get_field('header_type');

			if ( $headerType == 'image_only' ):
				get_template_part('template-parts/PageHeader/FullBanner');

				elseif ( $headerType == 'content_image' ):
				get_template_part('template-parts/PageHeader/ContentImageBanner');

				elseif ( $headerType == 'content_right_aligned_image' ):
				get_template_part('template-parts/PageHeader/ContentImageBanner','RightAligned');


				elseif ( $headerType == 'homepage_header' ):
				get_template_part('template-parts/PageHeader/HomeBanner');

			else:
				get_template_part('template-parts/PageHeader/NoHeader');

			endif;

		
	?>
	<main>
			
