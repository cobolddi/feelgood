<?php

/**
Template Name: Home Page Template
@Author: Raunaque Zamir
@email: raunaquethedeveloper@gmail.com
*/

	get_header();
?>

<?php 
	
	@include ('template-parts/PageHeader/HomeBanner.php');

	
		if( have_rows('page_blocks') ):
	    while( have_rows('page_blocks') ) : the_row();

			if ( get_row_layout() == 'left_content_right_image' ):
				get_template_part('template-parts/LeftContentRightImage');

			endif;

		endwhile;
		endif;


?>

<?php
	get_footer();
?>
