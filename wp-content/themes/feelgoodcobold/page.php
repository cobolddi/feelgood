<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package feelgoodCobold
 */

get_header();
?>

<?php
	if( have_rows('page_blocks') ):
    while( have_rows('page_blocks') ) : the_row();

		if ( get_row_layout() == 'left_content_right_aligned_image' ):
			get_template_part('template-parts/LeftContent', 'RightAlignedImage');

			elseif ( get_row_layout() == 'right_content_left_aligned_image' ):
			get_template_part('template-parts/RightContent', 'LeftAlignedImage');

			elseif ( get_row_layout() == 'left_content_right_image' ):
			get_template_part('template-parts/LeftContent','RightImage');


			elseif ( get_row_layout() == 'right_content_left_image' ):
			get_template_part('template-parts/RightContent', 'LeftImage');

		elseif ( get_row_layout() == 'animation_slider' ):
			get_template_part('template-parts/HomeBottomSlider');

			elseif ( get_row_layout() == 'testimonial_slider' ):
			get_template_part('template-parts/TestimonialSection');

		endif;

	endwhile;
	endif;

?>

<?php
get_footer();
