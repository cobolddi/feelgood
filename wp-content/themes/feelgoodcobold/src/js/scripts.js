$(document).ready(function(){

    // Bottom carousel

    $('#SVGAnimationSlider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        lazyLoad: 'ondemand',
        fade: true,
        focusOnSelect: true,
        asNavFor: '.slider-nav',
        responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '0px'
          }
        }]
   });

    // Bottom Slider Navigation

    $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        arrows: false,
        focusOnSelect: true,
        infinite: true,
        centerMode: true,
        adaptiveHeight: true,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 768,
          settings: {
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 3,
            slidesToScroll: 1
          }
        }]
    });

   // Testimonial Slider
    $(".testimonialSlider").slick({
        dots: true,
        vertical: false,
        centerMode: true,
        slidesToShow: 1,
        fade: true,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }]
    });

    // Product Slider
    $(".ProductMidslider").slick({
        dots: false,
        vertical: false,
        centerMode: true,
        slidesToShow: 1,
        fade: true,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            dots: true,
            fade: false,
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }]
    });


Headspace(document.querySelector('header'));

//Accordion
function accordion() {
    var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        // Variables privadas
        var links = this.el.find('.js-accordion-link');
        // Evento
        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el,
            $this = $(this),
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('is-open');

        if (!e.data.multiple) {
            $el.find('.js-accordion-submenu').not($next).slideUp().parent().removeClass('is-open');
            $el.find('.js-accordion-submenu').not($next).slideUp();
        };
    }
    var accordion = new Accordion($('#accordion'), false);
}
accordion();

    Headspace(document.querySelector('header'))

     // Product Feature Slider

    $('.ProductFeature').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        focusOnSelect: true,
        infinite: true,
        centerMode: true,
        centerPadding: '10px',
        adaptiveHeight: true,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 768,
          settings: {
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }]
    });

// Isotope filter Blog post

var $grid = $("#AllPostCategories").isotope({
    itemSelector: '.PostCategories',
    layoutMode: 'masonry',
    percentPosition: true,
    stagger: 30,
    hiddenStyle: { transform: 'translateY(100px)', opacity: 0 },
  });
  // filter items on button click
  $('.CategoriesFilterButton').on( 'click', 'button', function() {
    var filterValue = $(this).attr('data-filter');
    console.log(filterValue);
    $grid.isotope({ filter: filterValue });
    $('.CategoriesFilterButton button').removeClass('is_active');
    $(this).addClass('is_active');
  });


//DATERANGE PICKER
$('input[name="datefilter"]').daterangepicker({
    singleDatePicker: true,
    "linkedCalendars": false,
    minDate: moment(),
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY'));
  });


//AOS Animation on Scroll
AOS.init({
  duration: 1200,
})





//AOS Animation on Scroll
AOS.init({
  duration: 1200,
})


//Vivus SVG animation
var AnimateSvg1 = new Vivus('AnimateSvg1', { 
  duration: 200,
  // file: 'assets/img/temp-img/Slider-Zero-G.svg',
  // onReady: function (myVivus){
  //   myVivus.el.setAttribute('height', 'auto');
  // }
});
AnimateSvg1
.stop()
.reset();

new Vivus('AnimateSvg2', { 
  duration: 200,
  // forceRender: true,
  // file: 'assets/img/temp-img/Slider-tv-position.svg',
}).reset();

new Vivus('AnimateSvg3', { 
  duration: 200,
}).reset();

new Vivus('AnimateSvg4', { 
  duration: 200,
}).reset();

new Vivus('AnimateSvg5', { 
  duration: 200,
}).reset();

new Vivus('AnimateSvg6', { 
  duration: 200,
}).reset();

new Vivus('AnimateSvg7', { 
  duration: 200,
}).reset();

new Vivus('AnimateSvg8', { 
  duration: 200,
}).reset();

new Vivus('AnimateSvg9', { 
  duration: 200,
}).reset();



 });