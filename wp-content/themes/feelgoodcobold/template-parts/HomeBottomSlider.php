<section class="Section HomeBottomSlider">
	<div class="container">
		<h2><?php the_sub_field('heading'); ?></h2>
		<p><?php the_sub_field('sub_heading'); ?></p>

		<?php if( have_rows('animation_main_slider') ): ?>
		<div class="slider-for" id="SVGAnimationSlider">
			<?php 
				while( have_rows('animation_main_slider') ): the_row();
					$svgId++;
					$svg = get_sub_field('svg_image');
			?>
		    <div class="pic">
				<object class="svg" id="AnimateSvg<?php echo $svgId; ?>" type="image/svg+xml" data="<?php echo $svg['url']; ?>"></object>
				<h4><?php the_sub_field('svg_heading'); ?></h4>
				<p><?php the_sub_field('svg_content_text'); ?></p>
		    </div>
		    <?php endwhile; ?>
		    <!-- <div class="pic">
				<object class="svg" id="AnimateSvg2" type="image/svg+xml" data="assets/img/temp-img/Slider-tv-position.svg"></object>
				<h4>Bad Back</h4>
				<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release.</p>
		    </div>
		    <div class="pic">
				<object class="svg" id="AnimateSvg3" type="image/svg+xml" data="assets/img/temp-img/Slider-Foot_up_1.svg"></object>
				<h4>ZEERO G Position</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat quod, libero tenetur reiciendis suscipit temporibus ipsa, voluptatibus nemo enim, officiis illum cumque iusto quis magni.</p>
		    </div>
		    <div class="pic">
				<object class="svg" id="AnimateSvg4" type="image/svg+xml" data="assets/img/temp-img/Slider-lounge.svg"></object>
				<h4>Bad Back</h4>
				<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release.</p>
		    </div>
		    <div class="pic">
				<object class="svg" id="AnimateSvg5" type="image/svg+xml" data="assets/img/temp-img/Slider-massage.svg"></object>
				<h4>ZEERO G Position</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat quod, libero tenetur reiciendis suscipit temporibus ipsa, voluptatibus nemo enim, officiis illum cumque iusto quis magni.</p>
		    </div>
		    <div class="pic">
				<object class="svg" id="AnimateSvg6" type="image/svg+xml" data="assets/img/temp-img/Slider-tv-position.svg"></object>
				<h4>Bad Back</h4>
				<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release.</p>
		    </div>
		    <div class="pic">
				<object class="svg" id="AnimateSvg7" type="image/svg+xml" data="assets/img/temp-img/Slider-Foot_up_1.svg"></object>
				<h4>ZEERO G Position</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat quod, libero tenetur reiciendis suscipit temporibus ipsa, voluptatibus nemo enim, officiis illum cumque iusto quis magni.</p>
		    </div>
		    <div class="pic">
				<object class="svg" id="AnimateSvg8" type="image/svg+xml" data="assets/img/temp-img/Slider-lounge.svg"></object>
				<h4>Bad Back</h4>
				<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release.</p>
		    </div>
		    <div class="pic">
				<object class="svg" id="AnimateSvg9" type="image/svg+xml" data="assets/img/temp-img/Slider-tv-position.svg"></object>
				<h4>ZEERO G Position</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat quod, libero tenetur reiciendis suscipit temporibus ipsa, voluptatibus nemo enim, officiis illum cumque iusto quis magni.</p>
		    </div> -->
		</div>
		<?php endif; ?>

		<?php if( have_rows('thumbnail_slider') ): ?>
		<div class="slider-nav">
			<?php 
				while( have_rows('thumbnail_slider') ): the_row();
					$thumb = get_sub_field('thumbnail_image');
			?>
			<div class="thumb-pic">
				<img src="<?php echo $thumb['url']; ?>"><p><?php the_sub_field('thumbnail_text'); ?></p>				
			</div>
			<?php endwhile; ?>
			<!-- <div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Zero-G</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Head up</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Lounge</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Gym Junkie</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Massage</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Gym Junkie</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Jon Smith</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Gym Junkie</p>				
			</div> -->
		</div>
		<?php endif; ?>
	</div>
</section>