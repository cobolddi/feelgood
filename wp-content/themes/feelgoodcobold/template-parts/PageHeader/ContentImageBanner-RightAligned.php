<section class="ContentImageBannerRightAligned" style="background-color: #f5fafd;">
	<div class="SideAllignedImageDiv">
		<img src="assets/img/temp-img/banner-img.jpg">
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="PageBannerContentBlock">
					<h2>Contour Elite</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu vehicula nunc. Pellentesque sodales sit amet orci at placerat. Pellentesque condimentum lorem non quam venenatis, ut tincidunt augue feugiat. Vivamus eu lacus egestas, vestibulum risus eu, pharetra sem. Phasellus fringilla finibus orci, sed mollis sapien auctor id. Curabitur vel sapien ac elit gravida dapibus. Aenean ullamcorper enim ut dapibus viverra.</p>
					<!-- <p class="ButtonElement"><a href="" class="Button"><span>Discover</span></a></p> -->
				</div>
			</div>
		</div>
	</div>
</section>