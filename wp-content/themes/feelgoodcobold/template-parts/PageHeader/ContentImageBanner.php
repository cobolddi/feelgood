<section class="ContentImageBanner" style="background-color: #f5fafd;">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="PageBannerContentBlock">
					<h2>Frequently Asked Questions</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu vehicula nunc. Pellentesque sodales sit amet orci at placerat. Pellentesque condimentum lorem non quam venenatis, ut tincidunt augue feugiat. Vivamus eu lacus egestas, vestibulum risus.</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="PageBannerImageBlock">
					<img src="assets/img/temp-img/banner-img.jpg">
				</div>
			</div>
		</div>
	</div>
</section>