<!-- <section class="HomeBanner M-Top145" style="background-color: #f5fafd;">
	<div class="SideAllignedImageDiv">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/temp-img/banner-img.png">
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="HomeBannerContentBlock">
					<h1>We help people Live healthier Live happier</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus, magnam.</p>
					<p class="ButtonElement"><a href="" class="Button"><span>Discover</span></a></p>
				</div>
			</div>
		</div>
	</div>
</section> -->

<section class="HomeBanner M-Top145" style="background-color: <?php the_field('background_color'); ?>;">
	<div class="SideAllignedImageDiv">
		<img src="<?php the_field('homepage_header') ?>" alt="">
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="HomeBannerContentBlock">
					<h1><?php the_field('banner_heading'); ?></h1>
					<p><?php the_field('banner_conent_text') ?></p>

					<?php $link = get_field('banner_button'); 
					if($link): ?>
					<p class="ButtonElement">
						<a href="<?php echo $link; ?>" class="Btn_SolidBackground Btn-Arrow">
							<span>
								<?php echo the_field('button_name') ?>
								<!-- Discover -->
								<svg>
									<use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/contour.svg#icon-button-arrow-white"></use>
								</svg>
							</span>
						</a>
					</p>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
