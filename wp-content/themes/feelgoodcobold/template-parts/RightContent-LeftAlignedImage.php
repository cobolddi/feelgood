<section class="Section">
	<div class="RightContentLeftAlignedImage">
		<div class="SideAlignedImageDiv LeftAlignedImage">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/temp-img/matters.png">
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-6 ML-Auto">
					<div class="ContentBlock">
						<h2><?php the_sub_field('heading'); ?></h2>
						<?php the_sub_field('content_text'); ?>
						<p class="ButtonElement">
							<a href="<?php the_sub_field('button_link'); ?>" class="Btn_SolidBackground Btn-Arrow">
								<span>
									Discover
									<svg>
										<use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/contour.svg#icon-button-arrow-white"></use>
									</svg>
								</span>
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
