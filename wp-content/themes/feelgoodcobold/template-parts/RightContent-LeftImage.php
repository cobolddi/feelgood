<section class="Section RightContentLeftImage">
	<div class="container">
		<h2><?php the_sub_field('heading'); ?></h2>
		<div class="row">
			<div class="col-12 col-md-6">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/temp-img/about-us-mg.png">
			</div>
			<div class="col-12 col-md-6">
				<div class="RightContentBlock">
					<h4><?php the_sub_field('subheading'); ?></h4>
					<?php the_sub_field('content_text'); ?>

					<p class="ButtonElement">
						<a href="<?php the_sub_field('button_link'); ?>" class="Btn_SolidBackground Btn-Arrow">
							<span>
								Know More
								<svg>
									<use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/img/contour.svg#icon-button-arrow-white"></use>
								</svg>
							</span>
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>