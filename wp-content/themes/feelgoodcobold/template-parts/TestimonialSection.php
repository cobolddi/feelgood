<section class="Section TestimonialBlock" style="background-color: <?php the_field('background_color'); ?>; ">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
		<h2><?php the_sub_field('heading'); ?></h2>

		<?php if( have_rows('testimonial') ): ?>
		<div class="TestimonialSlider">
			<div class="testimonialSlider slider">
				<?php 
					while( have_rows('testimonial') ): the_row();
						$img = get_sub_field('client_image');
				?>
				<div class="SlidesBlock">
					<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" >
					
					<h4><?php the_sub_field('client_name'); ?></h4>
					<span><?php the_sub_field('company_name'); ?></span>
					<p><?php the_sub_field('content_text'); ?></p>
				</div>
				<?php endwhile; ?>
			</div>
		</div>
		<?php endif; ?>
	</div>
	</div>
	</div>
</section>